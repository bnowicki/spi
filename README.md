This library provides SPI ([Serial Peripheral Interface](https://en.wikipedia.org/wiki/Serial_Peripheral_Interface)) support for Mongoose OS with SPI Slave Mode for ESP32.
